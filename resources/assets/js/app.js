
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import App from './views/App'
import Home from './views/Home'
import About from './views/About'

const router = new VueRouter({
  mode: 'history',
  routes: [
    {path: '/', name: 'home', component: Home},
    {path: '/about', name: 'about', component: About},
  ]
})

// require('./bootstrap');

// window.Vue = require('vue');

const app = new Vue({
    el: '#app',
    components: { App },
    router
});
