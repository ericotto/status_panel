
# Dependencies


# To start server
```
~$ npm run production
```

# To stop server
```
~$ php artisan down
```

# Server Config

cd /srv
git clone https://ericotto@bitbucket.org/ericotto/status_panel.git
cd /status_panel

bash provision.sh

vim /etc/apache2/sites-available/status_panel.conf

NameVirtualHost SERVER_IP_ADDRESS
Listen 8080

<VirtualHost SERVER_IP_ADDRESS>
    ServerAdmin admin@example.com
    ServerName statuspanel.me
    ServerAlias www.statuspanel.me
    DocumentRoot /srv/status_panel/public

    <Directory /srv/status_panel/public/>
            Options Indexes FollowSymLinks MultiViews
            AllowOverride All
            Order allow,deny
            allow from all
            Require all granted
    </Directory>

    LogLevel debug
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

sudo a2dissite 000-default.conf
sudo a2ensite status_panel.conf
systemctl restart apache2
