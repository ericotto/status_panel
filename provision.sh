
sudo apt-get -y update
sudo apt-get -y upgrade

sudo apt-get install -y apache2

sudo apt-get install -y python-software-properties

sudo add-apt-repository -y ppa:ondrej/php
sudo apt-get install -y php7.1 php7.1-xml php7.1-mbstring php7.1-mysql php7.1-json php7.1-curl php7.1-cli php7.1-common php7.1-mcrypt php7.1-gd libapache2-mod-php7.1 php7.1-zip

curl -sS https://getcomposer.org/installer
sudo php -- --install-dir=/usr/local/bin --filename=composer

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs build-essential npm

cd /srv/status_panel

npm install cross-env
npm install

touch .env
cp .env.example .env
php artisan key:generate

systemctl restart apache2
